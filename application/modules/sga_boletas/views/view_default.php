<h3 class="page-header"><span class="glyphicon glyphicon-tags"></span>&nbsp;&nbsp;Boleta de Notas</h3>
<div id="mensaje"></div>
<hr/><br/> 
<center>

    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">Comunicado!</h4>
        <p>Por Seguridad este módulo solo esta permitido a usuarios Administradores y Coordinadores del Colegio. Si tuviese alguno de esos perfiles comuniquese con el WebMaster</p>
        <p class="mb-0">Cualquier inconveniente comuniquese con info@sistemas-dev.com</p>
    </div>

</center>