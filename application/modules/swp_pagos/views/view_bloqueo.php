<h3 class="page-header"><span class="glyphicon glyphicon-tags"></span>&nbsp;&nbsp;Pago por Caja</h3>
<div id="mensaje"></div>
<hr/><br/> 
<center>

    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">Comunicado!</h4>
        <p>Por seguridad solo se permite ingresar a esta opcion en el Horario de 07:00 am  a 20:00 pm. En caso que estuviese dentro del Horario permitido comuniquese con el Administrador.</p>
        <p class="mb-0">Cualquier inconveniente comuniquese con info@sistemas-dev.com</p>
    </div>

</center>