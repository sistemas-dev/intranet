<h3 class="page-header"><span class="glyphicon glyphicon-tags"></span>&nbsp;&nbsp;Asistencias</h3>
<div id="mensaje"></div>
<hr/><br/> 
<center>

    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">Comunicado!</h4>
        <p>Las asistencias  de Alumnos solo esta habilitado por el momento para el nivel <b>SECUNDARIA</b>. Si Ud. Tiene algun hijo en el Nivel Secundaria comuniquese con el Area de Sistemas de MARIANISTA para que le habiliten la verificacion de asistencias de su Hijo(a).</p>
        <p class="mb-0">Cualquier inconveniente comuniquese con <b>info@sistemas-dev.com</b></p>
    </div>

</center>